import random

def get_requirements():
    print("Random Number Generator")

    print("\nProgram Requirements:\n" 
        + "\n1. Get user beginning and ending integer values, and store in two variables."
        + "\n2. Display 10 random numbers between, and including, above values."
        + "\n3. Must use integer data types."
        + "\n4. Example 1: Using range() and randint() functions."
        + "\n5. Example 2: Using a list with range() and shuffle() functions.")

def random_num_generator():
    beginning_num = 0
    ending_num = 0

    print("Input:")
    beginning_num = int(input("Enter beginning value: "))
    ending_num = int(input("Enter ending value: "))

    print("\nOutput: ")
    print("Example 1: Using range() and randint() functions:")

    for i in range(10):
        print(random.randint(beginning_num, ending_num), sep = ", ", end = " ")


    print("\n\nExample 2: Using a list, with range() and shuffle() functions:")
    r = list(range(beginning_num, ending_num + 1))
    random.shuffle(r)
    for i in r:
        print(i, sep = ", ", end = " ")

    print()
        