import randomnum as r 

def main():
    r.get_requirements()
    r.random_num_generator()

if __name__ == "__main__":
    main()