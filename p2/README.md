# LIS4369

## Michael DeMaria

### PROJECT 2 Requirements:
1. Use Assignment 5 screenshots and R Manual to backward-engineer the following requirements:
2. Resources:
 	a. R Manual: https://cran.r-project.org/doc/manuals/r-release/R-lang.pdf
 	b. R for Data Science: https://r4ds.had.co.nz/
3. Use Motor Trend Car Road Tests data:
 	a. Research the data! https://stat.ethz.ch/R-manual/R-devel/library/datasets/html/mtcars.html
 	b. url = "http://vincentarelbundock.github.io/Rdatasets/csv/datasets/mtcars.csv"

*Deliverables:*
#### README.md file should include the following items:

* Screenshots of plots produced in from R Script R Studio using ggplot library
* One graph using qplot()
* One graph using plot()


#### Assignment Screenshots:

*Screenshot of qplot() graph:*
![Plot 1](disp_and_mpg.png)

*Screenshot of plot() graph:*
![Plot 2](weight_v_mpg.png)

*Screenshot of code example:*
![R Code](R_code_example.png)
