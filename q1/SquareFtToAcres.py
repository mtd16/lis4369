#simple program to convert square feet to acres
#by Mike DeMaria
def GetRequirements():
    print("Square Feet to Acres")

    print("\nProgram Requirements:"
        + "\n1. Research: number of square feet to acres of land."
        + "\n2. Must use float data type for user input and calculation."
        + "\n3. Format and round conversion to two decimal places.")

def Calculate_sqft_to_acre():
    acres = 0
    sqft = 0
   
    #constant 
    SQ_FT_PER_ACRE = 43560
    
    print("Input:")
    sqft = float(input("Enter square feet: "))
    acres = sqft / SQ_FT_PER_ACRE

    print("\nOutput:")
    print("{0:,.2f} {1} {2:,.2f} {3}".format(
        sqft, " square feet = ", acres, " acres"))