my_list = []

def get_requirements():
    print("Python Lists\n")
    print("Program Requirements:\n"
        + "1. Lists (Python data structure): mutable, ordered sequence of elements.\n"
        + "2. Lists are mutable/changeable--that is, can insert, update, delete.\n"
        + "3. Create list - using square brackets [list]: my_list = [\"cherries\", \"apples\", \"bananas\", \"oranges\"].\n"
        + "4. Create a program that mirrors the following IPO (input/processing/output) format).\n"
        + "Note: user enters number of requested list elements, dynamically rendered below (that is, number of elements can change each run).\n")

def enter_list():
    global my_list
    print("Input: ")
    list_size = int(input("Enter number of list elements: "))
    

    print()

    for i in range(1, list_size + 1):
       element = str(input("{} {}: ".format("Please list element", i)))
       my_list.append(element)

    print()

    print("Output:")
    print("Print my_list: ")
    print(my_list)

    print()

def insert():
    global my_list
    insert_elem = input("Please enter list element: ")
    index = int(input("Please enter list *index* position (note: must convert to int): "))
    my_list.insert(index, insert_elem)

    print()

    print("Insert element into specific position in my_list")
    print(my_list)

    print()

    print("Count number of elements in list: ")
    print(len(my_list))

    print()

def sort():
    global my_list
    print("Sort elements in list alphabetically:")
    my_list = sorted(my_list)

    print(my_list)

    print()

def reverse():
    global my_list
    print("Reverse list: ")
    
    my_list.reverse()

    print(my_list)

    print()

def remove_last():
    global my_list
    print("Remove last list element: ")
    my_list.pop()
    print(my_list)

    print()

def delete_by_index():
    global my_list
    print("Delete second element from list by *index* (note: 1 = 2nd element):")
    del my_list[1]

    print(my_list)

    print()

def delete_by_value():
    global my_list
    print("Delete element from list by *value* (cherries):")
    my_list.remove('cherries')

    print(my_list)

    print()

def delete_list():
    global my_list
    print("Delete all elements from list:")
    my_list.clear()
    print(my_list)    