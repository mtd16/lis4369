import lists as l 

def main():
    l.get_requirements()
    l.enter_list()
    l.insert()
    l.sort()
    l.reverse()
    l.remove_last()
    l.delete_by_index()
    l.delete_by_value()
    l.delete_list()

if __name__ == "__main__":
    main()