print("Calorie Percentage")
print("\nProgram Requirements:"
    + "\n1. Find calories per grams of fat, carbs, and protein."
    + "\n2. Calculate percentages."
    + "\n3. Must use float data types."
    + "\n4. Format, right-align numbers, and round to two decimal places.")

print("\nInput:")
fat = float(input("Enter total fat grams: "))
carb = float(input("Enter total carb grams: "))
protein = float(input("Enter total protein grams: "))

fat_cal = fat * 9
carb_cal = carb * 4
pro_cal = protein * 4

total_cal = fat_cal + carb_cal + pro_cal

print("\nOutput: ")
print("{0:8} {1:>10} {2:>13}".format(
    "Type", "Calories", "Percentage"))
print("{0:8} {1:10,.2f} {2:13.2%}".format(
    "Fat", fat_cal, fat_cal / total_cal))
print("{0:8} {1:10,.2f} {2:13.2%}".format(
    "Carbs", carb_cal, carb_cal / total_cal))
print("{0:8} {1:10,.2f} {2:13.2%}".format(
    "Protein", pro_cal, pro_cal / total_cal))