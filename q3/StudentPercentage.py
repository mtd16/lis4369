#simple program to calculate percentage of IT/ICT students in class
#by Mike DeMaria

print("IT/ICT Student Percentage")
print("\nProgram Requirements: "
    + "\n1. Find number of IT/ICT students in class."
    + "\n2. Calculate IT/ICT Student Percentage."
    + "\n3. Must use float data type (to facilitate right-alignment)."
    + "\n4. Format, right-align numbers, and round to two decimal places.")

print("\nInput:")
it_students = float(input("Enter number of IT students: "))
ict_students = float(input("Enter number of ICT students: "))

total_stu = it_students + ict_students

print("\nOutput: ")
print("{0:17} {1:>5.2f}".format("Total Students:", total_stu))
print("{0:17} {1:>5.2%}".format("IT Students:", it_students / total_stu))
print("{0:17} {1:>5.2%}".format("ICT Students:", ict_students / total_stu))
