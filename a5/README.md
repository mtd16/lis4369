# LIS4369

## Michael DeMaria

### Assignment 5 Requirements:

*Deliverables:*

1. R Commands: save a file of all the R commands included in the tutorial.
2. R Console: save a screenshot of some of the R commands executed above(below requirement).
3. Graphs: save at least 5 separate image filesdisplaying graph plotscreated from the tutorial.
4. RStudio: save onescreenshot (similar to the one below), displaying the following 4 windows:
    - R source code(top-left corner)
    - Console(bottom-left corner)
    - Environment(or History), (top-right corner)
    - Plots(bottom-right corner)

#### README.md file should include the following items:

* Screenshot of Titanic data analysis in RStudio
* Screenshot of completed R tutorial


#### Assignment Screenshots:

#### Part 1

*Screenshot of R code output 1:*
![Titanic Data 1](img/titanic_1.png)

*Screenshot of R code output 2:*
![Titanic Data 2](img/titanic_2.png)


### Part 2

*R commands running:* 
![R commands](img/r_commands.png)

*Screenshot of 5 different plots*
![Plot 1](img/plot_1.png)
![Plot 2](img/plot_2.png)
![Plot 3](img/plot_3.png)
![Plot 4](img/plot_4.png)
![Plot 5](img/plot_5.png)



