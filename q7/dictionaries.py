def get_requirements():
    print("Python Dictionaries")

    print("\nProgram Requirements:")
    print("\n1. Dictionaries (Python data structure): unordered key:value pairs.")
    print("\n2. Dictionary: an associative array (also known as hashes).")
    print("\n3. Any key in a dictionary is associated (or mapped) to a value (i.e., any Python data type).")
    print("\n4. Keys: must be of immutable type (string, number or tuple with immutable elements) and must be unique.")
    print("\n5. Values: can be any data type and can repeat.")
    print("\n6. Create a program that mirrors the following IPO (input/process/output) format.")
    print("\tCreate empty dictionary, using curly braces {}: my_dictionary = {}")
    print("\tUse the following keys: fname, lname, degree, major, gpa")
    print("Note: Dictionaries have key-value pairs instead of single values; this differentiates a dictionary from a set.")

def dictionary_function():

    my_dictionary = {}

    print("\nInput:")
    first_name = input("First Name: ")
    last_name = input("Last Name: ")
    degree = input("Degree: ")
    major = input("Major (IT or ICT):")
    gpa = float(input("GPA: "))

    my_dictionary = {
        "first_name": first_name,
        "last_name": last_name,
        "degree": degree,
        "major": major,
        "GPA": gpa
    }

    print("\nOutput: ")
    print("Print my_dictionary:")
    print(my_dictionary)
    print()

    print("Return view of dictionary (key, value) paur, built-in function:")
    key_values = my_dictionary.items()
    print(key_values)
    print()

    print("Return view object of all keys, built-in function")
    keys = my_dictionary.keys()
    print(keys)
    print()

    print("Return view object of all values in dictionary, built in function")
    values = my_dictionary.values()
    print(values)
    print()

    print("Print only first and last names, using get() function")
    first = my_dictionary.get("first_name")
    last = my_dictionary.get("last_name")
    print(first + " " + last)
    print()

    print("Count number of items in dictionary")
    items = len(my_dictionary)
    print(items)
    print()

    print("Remove last dictionary item (popitem): ")
    my_dictionary.popitem()
    print(my_dictionary)
    print()

    print("Delete major from dictionary, using key:")
    my_dictionary.pop('major')
    print(my_dictionary)
    print()

    print("Return object type")
    object_type = type(my_dictionary)
    print(object_type)
    print()

    print("Delete all items from list")
    my_dictionary.clear()
    print(my_dictionary)

