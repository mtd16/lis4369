> **NOTE:**/ This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Michael DeMaria

### Project 1 Requirements:

*Sub-Heading:*

1. Code and run demo.py, making sure necessary packages are installed.
2. Use it to backward-engineer the screenshots from class.

#### README.md file should include the following items:

* Screenshot of running Data Analysis 1 program.
* Screenshot of graph running Exxon Mobil stock analysis data.
* Data should be scraped from Yahoo! Finance

#### Assignment Screenshots:

*Screenshot of Data Analysis 1 program:*

![Data Analysis 1 Screenshot](p1_data_analysis_1/img/data_analysis_1_1.png)
![Data Analysis 1 Screenshot](p1_data_analysis_1/img/data_analysis_1_2.png)
![Data Analysis 1 Screenshot](p1_data_analysis_1/img/data_analysis_1_3.png)


![Stock Analysis Graph Screenshot](p1_data_analysis_1/img/graph.png)




