import pandas as pd
import datetime
import pandas_datareader as pdr 
import matplotlib.pyplot as plt 
from matplotlib import style

def get_requirements():
    print("Data Analysis 1\n")

    print("Program Requirements:\n"
        + "1. Run demo.py\n"
        + "2. If errors, more than likely\n"
        + "3. Test Python Package Installer: pip freeze\n"
        + "4. Research how to do the following installations:\n"
        + "\ta. pandas (only if missing)\n"
        + "\tb. pandas data-reader (only if missing)\n"
        + "\tc. matplotlib (only if missing)\n"
        + "5. Create at least three functions that are called by the program:\n"
        + "\ta. main(): calls at least two other functions.\n"
        + "\tb. get_requirements(): displays the program requirements.\n"
        + "\tc. data_anaysis_1(): displays the following data")
    
def data_analysis_1():
    
    start = datetime.datetime(2010, 1, 1)
    end = datetime.datetime(2018, 10, 15)

    # NOTE: XOM is stock market symbol for Exxon Mobil Corporation
    df = pdr.DataReader("XOM", "yahoo", start, end)

    print("\nPrint number of records: ")
    print(len(df))

    # Why is it important to run the following print statement. . .
    print(df.columns)

    print("\nPrint data frame: ")
    print(df) # Note: for efficiency, only prints 60--note *all* records

    print("\nPrint first five lines: ")
    print(df.head())    # head() Prints top 5 rows. Here, with 7 columns

    print("\nPrint last five lines: ")
    print(df.tail())

    print("\nPrint first 2 lines: ")
    print(df.head(2))

    print("\nPrint last 2 lines: ")
    print(df.tail(2))

    # Research what these styles do!
    # style.use('fivethirtyeight')
    # compare with...
    style.use('dark_background')

    df['High'].plot()
    df['Adj Close'].plot()
    plt.legend()
    plt.show()