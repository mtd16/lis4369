> **NOTE:**/ This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Michael DeMaria

### Assignment 3 Requirements:

*Sub-Heading:*

1. Backward-engineer paint calculator program using Python and include screenshots.
2. Chapter 6 Questions

#### README.md file should include the following items:

* Screenshot of running Painting Estimator program

#### Assignment Screenshots:

*Screenshot of Painting Estimator program:*
![Painting Estimator screenshot](paint_estimator/img/paint_estimator.png)



