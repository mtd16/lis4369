def get_requirements():
    print("Paint Estimator")
    print("\nProgram Requirements:\n"
        + "\n1. Calculate home interior paint cost (w/o primer)."
        + "\n2. Must use float data types."
        + "\n3. Must use SQFT_PER_GALLON constant (350)."
        + "\n4. Must use iteration structure (aka \"loop\")."
        + "\n5. Format, right-align numbers, and round to two decimal places."
        + "\n6. Create at least three functions that are called by the program:"
        + "\n\ta. main(): calls at least two other functions."
        + "\n\tb. get_requirements(): displays the program requirements."
        + "\n\tc. estimate_painting_cost(): calculate interior home painting.\n")

def estimate_painting_cost():
    SQFT_PER_GALLON = 350
    total_sqft = 0.0

    print("\nInput:")
    total_sqft = float(input('Enter total interior sq ft: '))
    price_per_gallon = float(input('Enter price per gallon paint: '))
    rate = float(input('Enter hourly painting rate per sq ft: '))

    num_gallons = total_sqft / SQFT_PER_GALLON
    paint_cost = price_per_gallon * num_gallons
    labor_cost = total_sqft * rate
    total_cost = paint_cost + labor_cost

    print("\nOutput:")
    print("{0} {1:>25}".format("Item", "Amount"))
    print("{0} {1:>17,.2f}".format("Total Sq Ft:", total_sqft))
    print("{0} {1:>12.2f}".format("Sq Ft per Gallon:", SQFT_PER_GALLON))
    print("{0} {1:>11.2f}".format("Number of Gallons:", num_gallons))
    print("{0} {1:>4} {2:>7,.2f}".format("Paint per Gallon:", "$", price_per_gallon))
    print("{0} {1:>5} {2:>7,.2f}".format("Labor per Sq Ft:", "$", rate))

    print("{0:12} {1:9} {2}".format("\nCost", "Amount", "Percentage"))
    print("{0:8} {1:2} {2:,.2f} {3:>13.2%}".format(
        "Paint:", "$", price_per_gallon * num_gallons, paint_cost / total_cost))
    print("{0:8} ${1:,.2f} {2:13.2%}".format(
        "Labor", labor_cost, labor_cost / total_cost))
    print("{0:8} ${1:,.2f} {2:13.2%}".format(
        "Total:", total_cost, (paint_cost + labor_cost) / total_cost))
