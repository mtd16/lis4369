import painting as p

def main():
    p.get_requirements()
    check = "y"

    while check.lower() == "y":
        p.estimate_painting_cost()
        check = input("\nEstimate another paint job? (y/n): ")

    print("\nThank you for using our Painting Estimator!")
    print("Please visit our web site: http://www.mysite.com")

if __name__ == "__main__":
    main()
