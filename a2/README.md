> **NOTE:**/ This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Michael DeMaria

### Assignment 2 Requirements:

*Sub-Heading:*

1. Backward-engineer payroll program using Python
2. Chapter Questions (Ch 4)

#### README.md file should include the following items:

* Screenshots of no overtime and overtime payroll programs

#### Assignment Screenshots:

*Screenshot of no overtime payroll program:*
![IDLE Payroll Screenshot](payroll/img/no_overtime.png)


*Screenshot of overtime payroll program*:
![IDLE Visual Studio Code Screenshot](payroll/img/overtime.png)


