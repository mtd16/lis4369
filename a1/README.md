> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Mike DeMaria

### Assignment 1 Requirements:

*Four Part:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Questions
4. Bitbucket repo links:
* this assignment and
* the completed tutorial (bitbucketstation locations)

#### README.md file should include the following items:

* Screenshots of a1_tip_calculator application running
* git commands w/short descriptions
 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init- create a new local repository
2. git status- list files you have changed
3. git add- add one or more files to index
4. git commit- commit changes
5. git push- send changes to the master branch of your remote repos
6. git pull- fetch and merge changes on remote server to working
directory
7. git config- tell Git who you are

#### Assignment Screenshots:

*Screenshot of a1_tip_calculator application running (IDLE)* 

![IDLE a1_tip_calculator](img/a1_tip_calculator.png)

*Screenshot of a1_tip_calculator application running (Visual Studio Code)* 

![Visual Studio Code a1_tip_calculator](img/a1_tip_calculator_vscode.png)

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mtd16/bitbucketstationlocations/src "Bitbucket Station Locations")