import MilesPerGallon as m  

def main():
    m.GetRequirements()
    m.Calculate_miles_per_gallon()

if __name__ == "__main__":
    main()