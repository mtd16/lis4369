# LIS4369 - Extensible Enterprise Solutions

## Mike DeMaria

### LIS4369 Requirements:

*Course Work Links:*

1. [A1 README.md](https://bitbucket.org/mtd16/lis4381/src/master/a1/README.md)
    * Install Python
    * Install R
    * Install R Studio
    * Create a1_tip_calculator application
    * Provide screenshots of installations
    * Create Bitbucket repo
    * Complete Bitbucket tutorial (bitbucketstationlocations)
    * Provide git command descriptions
2. [A2 README.md](https://bitbucket.org/mtd16/lis4369/src/master/a2/README.md)
    * Screenshot of no overtime payroll program
    * Screenshot of overtime payroll program
3. [A3 README.md](https://bitbucket.org/mtd16/lis4369/src/master/a3/README.md)
    * Screenshot of Paint Estimator program to calculate the estimated cost of a paint job based on the cost of paint, labor, and size in square feet.
4. [P1 README.md](https://bitbucket.org/mtd16/lis4369/src/master/p1/README.md)
    * Screenshot of Project 1 Data Analysis program to display finance data scraped from Yahoo! Finance website
    * Screenshot of graph displayed using MatPlotLib
5. [A4 README.md](https://bitbucket.org/mtd16/lis4369/src/master/a4/README.md)
    * Titanic Data
6. [A5 README.md](https://bitbucket.org/mtd16/lis4369/src/master/a5/README.md)
    * Introduction to data analysis using R and RStudio
7. [P2 README.md](https://bitbucket.org/mtd16/lis4369/src/master/p2/README.md)
    * Reverse Engineer Motor Car Trend Vehicle Data
